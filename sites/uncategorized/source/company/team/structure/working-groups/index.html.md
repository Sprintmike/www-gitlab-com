---
layout: markdown_page
title: "Working Groups"
description: "Like all groups at GitLab, a working group is an arrangement of people from different functions. Learn more!"
canonical_path: "/company/team/structure/working-groups/"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## What's a Working Group?

Like all groups at GitLab, a [working group](https://en.wikipedia.org/wiki/Working_group) is an arrangement of people from different functions. What makes a working group unique is that it has defined roles and responsibilities, and is tasked with achieving a high-impact business goal. A working group disbands when the goal is achieved (defined by exit criteria) so that GitLab doesn't accrue bureaucracy.

### CEO Handbook Learning Discussion on Working Groups

GitLab's CEO, Sid, and Chief of Staff to the CEO, Stella, and the Learning & Development team discuss Working Groups in detail during a [CEO handbook learning session](/handbook/people-group/learning-and-development/learning-initiatives/#ceo-handbook-learning-sessions).

Topics covered include:
1. What is a working group
2. When to start a working group
3. Difference between project managers and working groups. 
   1. Note: GitLab does not internally have project managers. Individual team members should have agency and accountability. We don't want someone who just tracks the status of work and makes updates. We want people to do this directly, and we want the [DRI](/handbook/people-group/directly-responsible-individuals/) to own the process and outcome. This supports accountability. We sometimes have project managers when interacting with external organizations, because accountability is harder when working with external parties.
4. Lessons learned from previous working groups
5. Why and how working groups support cross-functional projects

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/tE3d8WUSL30" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

## Roles and Responsibilities

### Required Roles

**Facilitator**

Assembles the working group, runs the meeting, assigns action items to Functional Leads, and communicates results
* Meeting agendas should be organized to stay on topic. Follow up on goals from the previous meeting, and align back to the exit criteria in each meeting.
* After a long discussion about a topic, try to summarize it and result in an action item - Determine if this topic should be pursued further, or if it changes the exit criteria.
* Assign any actions, initiatives, or outstanding questions to a [DRI](/handbook/people-group/directly-responsible-individuals/) to investigate further. This ensures accountability and prevents overwhelming any single member.
* Consider using an [Issue Board](https://docs.gitlab.com/ee/user/project/issue_board.html) to track working group tasks by using the ~"WorkingGroup::" scoped label on each issue. This can be done separately from any other development related issues that the working group needs to track.

**Executive Stakeholder**

An Executive or [Senior Leader](/company/team/structure/#senior-leaders) interested in the results, or responsible for the outcome

**Functional Lead**

Someone who represents their entire function to the working group, regularly monitors the Working Group Slack Channel, creates issues for action items, serves as DRI for issues created, actively participates in meetings, volunteers for opportunities to further the Working Groups goals, regularly attends meetings either synchronously or asynchronously, shares information learned from the Working Group with their Functional teams, volunteers to take on action items , gathers feedback from Functional teams and brings that feedback back to the Working Group

### Optional Roles

**Member**

Any subject matter expert, attends meetings synchronously or asynchronously on a regular basis, regularly monitors the Working Group Slack Channel, shares information learned from the Working Group with their peers, and gathers feedback from their peers and brings that feedback back to the Working Group

## Guidelines

* An executive sponsor is required, in part, to prevent proliferation of working groups
* A person should not facilitate more than one concurrent working group
* Generally, a person should not be a part of more than two concurrent working groups in any role
* It is highly recommended that anyone in the working group with OKRs aligns them to the effort


## Process

* Preparation
  * Create a working group page
  * Assemble a team from required functions
    * Share in appropriate Slack channel(s) to encourage a diverse group of participants
  * Create an agenda doc public to the company
  * Create a Slack channel (with `#wg_` prefix) that is public to the company
  * Schedule a recurring Zoom meeting
* Define a goal and exit criteria
* Gather metrics that will tell you when the goal is met
* Organize activities that should provide incremental progress
* Ship iterations and track the metrics
* Communicate the results
  * Consider regular updates to the [#whats-happening-at-gitlab](https://gitlab.slack.com/archives/C0259241C)  slack channel as progress is made towards goal.
  * Communicate outcomes using [multi modal communication](/handbook/communication/#multimodal-communication).
  * Notify widely of exit outcomes via channels such as the [engineering week in review](/handbook/engineering/#communication).
* Disband the working group
  * Celebrate. Being able to close a working group is a thing to be celebrated!
  * Move the working group to the "Past Working Groups" section on this page
  * Update the working group's page with the close date and any relevant artifacts for prosperity
  * Archive the slack channel
  * Delete the recurring calendar meeting

## Modifications to Process for Limited Access Communications

We make things public by default because [transparency is one of our values](/handbook/values/#transparency).
Some things can't be made public and are either [internal](#internal) to the company or have [limited access](#limited-access) even within the company.
If something isn't on our [Not Public list](/handbook/communication/#not-public), we should make it available externally. If a working group is working on something on the Not Public List, working group team members should take precautions to limit access to information until it is determined that information can be shared more broadly. To highlight a few modifications to the process above:

1. Preparation
   1. Determine an appropriate project name using [limited access naming conventions](/handbook/communication/#limited-access).
   1. Create an overview page and add the link to [Active Working Groups](/company/team/structure/working-groups/#active-working-groups-alphabetic-order). You can share limited information, but capture key team members, including the facilitator, executive stakeholder, and functional lead.
   1. If working in the handbook, evaluate whether the page should be confidential or be housed in a new project with limited access. Consider working in the [staging handbook](/handbook/handbook-usage/#the-staging-handbook). We use this when information may need to be iterated on or MR branches may need to be created in staging before it is made public. Outside of E-Group, temporary access may be granted on a project-specific basis.
   1. Maintain a list of working group members and other folks who are participating in or informed of the project. This list should be available to all participating team members. Folks should not be added to this list until it is confirmed that they understand what can be communicated.
   1. Ensure that each working group team member understands what can be communicated externally and internally.
   1. Have private Slack channels that include folks who are directly working on the project.
   1. Limit the agenda to a specific set of folks.
1. Communicate the results
   1. Communicate results and progress to the direct working group or other key stakeholders to ensure that folks are aligned and have context on key happenings. Do not share sensitive information outside of private channels.
1. Proactively share information if the project is no longer limited access
   1. Notify widely of progress or exit outcomes when information can be shared more broadly.
   1. Evaluate which artifacts and communication material can be made internally available or public.
      1. If you were working in the [staging handbook](/handbook/handbook-usage/#the-staging-handbook), follow instructions to make a merge request against the public repo.
      1. Transition members to public Slack channels and archive private channels.
      1. Deprecate private agendas. Link this to a new agenda document.
      1. Consider making GitLab Groups and Projects public or avialable to a broader audience.

## Participating in a Working Group

If you are interested in participating in an active working group, it is generally recommended that you first communicate with your manager and the facilitator and/or lead of the working group. After that, you can add yourself to the working group member list by creating a MR against the specific working group handbook page.

## Active Working Groups (alphabetic order)

* [Architecture Kickoff](/company/team/structure/working-groups/architecture-kickoff/)
* [Category Leadership](/company/team/structure/working-groups/category-leadership/)
* [China Service](/company/team/structure/working-groups/china-service/)
* [Contribution Efficiency](/company/team/structure/working-groups/contribution-efficiency)
* [Database Scalability](/company/team/structure/working-groups/database-scalability/)
* [Event Stream](/company/team/structure/working-groups/event-stream/)
* [Expense Management](/company/team/structure/working-groups/expense-management/)
* [FedRAMP Execution](/company/team/structure/working-groups/fedramp-execution/)
* [Frontend Observability](/company/team/structure/working-groups/frontend-observability/)
* [GitLab.com Disaster Recovery](/company/team/structure/working-groups/disaster-recovery/)
* [GTM Product Analytics](/company/team/structure/working-groups/product-analytics-gtm/)
* [IACV - Delta ARR](/company/team/structure/working-groups/iacv-delta-arr/)
* [Improve Ops Quality](/company/team/structure/working-groups/improve-ops-quality/)
* [Issue Prioritization Framework](/company/team/structure/working-groups/issue-prioritization-framework/)
* [Learning Experience](/company/team/structure/working-groups/learning-experience/)
* [Merge Request Report Widgets](/company/team/structure/working-groups/merge-request-report-widgets/)
* [MLOps](/company/team/structure/working-groups/mlops/)
* [Multi-Large](/company/team/structure/working-groups/multi-large/)
* [Object Storage](/company/team/structure/working-groups/object-storage/)
* [Pipeline Validation Service Operations](/company/team/structure/working-groups/pipeline-validation-service-operations/)
* [Product Career Development Framework](/company/team/structure/working-groups/product-career-development-framework/)
* [Purchasing Reliability](/company/team/structure/working-groups/purchasing-reliability/)
* [SOX PMO](/company/team/structure/working-groups/sox/)
* [Talent Acquisition SSOT](/company/team/structure/working-groups/recruiting-ssot/)
* [Webpack (Frontend build tooling)](/company/team/structure/working-groups/webpack)

## Past Working Groups (alphabetic order)

* [CI Queue Time Stabilization](/company/team/structure/working-groups/ci-queue-stability/)
* [Commercial & Licensing](/company/team/structure/working-groups/commercial-licensing/)
* [Development Metrics](/company/team/structure/working-groups/development-metrics/)
* [Dogfood Plan](/company/team/structure/working-groups/dogfood-plan/)
* [Engineering Career Matrices](/company/team/structure/working-groups/engineering-career-matrices/)
* [Experimentation](/company/team/structure/working-groups/experimentation/)
* [Githost Migration](/company/team/structure/working-groups/githost-migration/)
* [GitLab.com Cost](/company/team/structure/working-groups/gitlab-com-cost/)
* [GitLab.com Revenue](/company/team/structure/working-groups/gitlab-com-revenue/)
* [gitlab-ui (CSS and Components)](/company/team/structure/working-groups/gitlab-ui/)
* [IC Gearing](/company/team/structure/working-groups/ic-gearing/)
* [Internal Feature Flag usage](/company/team/structure/working-groups/feature-flag-usage/)
* [Isolation](/company/team/structure/working-groups/isolation/)
* [Learning Restructure](/company/team/structure/working-groups/learning-restructure)
* [Licensing and Transactions Improvements](/company/team/structure/working-groups/licensing-transactions-improvements/)
* [Log Aggregation](/company/team/structure/working-groups/log-aggregation/)
* [Logging](/company/team/structure/working-groups/logging/)
* [Minorities in Tech (MIT) Mentoring Program](/company/team/structure/working-groups/mit-mentoring/)
* [Performance Indicators](/company/team/structure/working-groups/performance-indicators/)
* [Product Analytics](/company/team/structure/working-groups/product-analytics/)
* [Product Development Flow](/company/team/structure/working-groups/product-development-flow/)
* [Product Engagement Actions (FY21)](/company/team/structure/working-groups/FY21-product-engagement-actions/)
* [Real-Time](/company/team/structure/working-groups/real-time/)
* [Secure Offline Environment Working Group](/company/team/structure/working-groups/secure-offline-environment/)
* [Self-Managed Scalability](/company/team/structure/working-groups/self-managed-scalability/)
* [Sharding](/company/team/structure/working-groups/sharding/)
* [Simplify Groups & Projects](/company/team/structure/working-groups/simplify-groups-and-projects/)
* [Single Codebase](/company/team/structure/working-groups/single-codebase/)
* [Tiering](/company/team/structure/working-groups/tiering/)
* [Transient Bugs](/company/team/structure/working-groups/transient-bugs/)
* [Upstream Diversity](/company/team/structure/working-groups/upstream-diversity/)

## 11 Cross-Functional Initiatives   
In FY22-Q4, we identified 11 cross-functional initiatives that are key to GitLab's success in FY23 and beyond. While there are other important business initiatives and priorities that exist within functions or require engagement across the business, we elevated these initiatives to address cross-functional dependencies, align on goals, and ensure ongoing reporting and monitoring. 

### 11 Cross-Functional Initiative DRIs
Each of our top cross-functional initiatives has an executive sponsor and a [directly responsible individual (DRI)](/handbook/people-group/directly-responsible-individuals/). DRIs are the folks ultimately held accountable for the success (or failure) of the project. They are the linchpins as these initiatives require deep cross-functional alignment and coordination. DRIs are also responsible for performance tracking and reporting. This includes:
1. Providing updates in advance of relevant [Key Reviews](https://about.gitlab.com/handbook/key-review). Updates should map to the Executive Sponsor who is aligned with the initiative. For example, category leadership maps to Marketing as the Exec Sponsor is the CMO. The DRI should plan to update the project tracking sheet and designated overview slides in advance of the meeting.
1. Providing updates in Top Initiative Check In Meetings. No slides are needed, but the project tracking sheet should be updated in advance. DRIs should come prepared to speak to status of the initiatives and any notable risks and/or dependencies. 

### Information Access
The full tracking file is [limited access](/handbook/communication/#limited-access) as required to comply with [SAFE guidelines](/handbook/legal/safe-framework/). Team members with appropriate permissions have been given access to the initiative file. Team members with access should be captured in the first tab of the file. If team members with access want to add folks and are unsure about appropriate perimissions, they should confirm the addition with their E-Group Sponsor. They should then add the team members to the first tab before providing access to the file. 

Team members without access can find the initiatives by searching within Google Drive for "FY23 Cross-Functional Projects." The intention is to migrate this list to the internal handbook. 
