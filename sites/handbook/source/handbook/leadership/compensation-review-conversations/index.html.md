---
layout: handbook-page-toc
title: Compensation Review Conversations
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Compensation Review Conversations
Conversations with regards to compensation are an important part of being a people manager. This page will take you through information and recommendations to effectively manage and guide these conversations. If you’re ever in doubt or have a question, don't hesitate to reach out to your aligned [People Business Partner.](/handbook/people-group/#people-business-partner-alignment-to-division).
 
## Content to Review Prior to Compensation Conversations
Please make sure to review and understand the following content with regards to Compensation at GitLab:
* [GitLab’s Guide to Total Rewards](/handbook/total-rewards/)
* [Compensation Calculator](https://comp-calculator.gitlab.net/users/sign_in)
* [“How to Think about Your Equity”](https://www.youtube.com/watch?v=ooWIfdsK5ho&ab_channel=GitLabUnfiltered) video embedded in the Compensation Calculator linked above
* [Annual Compensation Review Cycle](/handbook/total-rewards/compensation/compensation-review-cycle/#determining-compensation-increases)
* [Equity Refresh Program/Stock Option page](/handbook/stock-options)
* [Talent Assessment at GitLab](/handbook/people-group/talent-assessment/)
 
## Total Rewards Statements
Starting FY22 Total Rewards will be providing [Total Rewards Statements](/handbook/total-rewards/compensation/compensation-review-cycle/#total-rewards-statement) outlining the Compensation package: cash, equity, and benefits. We recommend reviewing this letter before the conversation and use the letter as preparation to communicate exact approved figures. 
 
### Compensation Communication Recommendations 
 
* **Communicate the increase face-to-face over [Zoom](/handbook/communication/#zoom).** As a manager, this is an opportunity for you to have a conversation with your team member about their compensation, potential increase and/or equity refresh grant. Having the conversation over Zoom allows for you to have a dialogue with your team member (versus just sharing the number or percentage) and allows you to pick up other information like tone and non-verbal cues which can tell you more about how someone is feeling.
 
* **Prepare for the call ahead of time.** As a manager, you should have awareness of the following facts about GitLab's compensation principles (please review the handbook's [Global Compensation](/handbook/total-rewards/compensation/) page), any recent changes to the compensation calculator/band and personal details about your team member in BambooHR:
    * Hire date
    * Current compensation (including base salary, variable pay (where applicable), equity etc.)
    * Date of last compensation adjustment
    * Performance Factor
    * Key Talent 
    * Location factor changes
    * Benchmark changes
    * % and # increase: This number can be found on the Total Rewards Statement


* Communicate the change at the beginning of the meeting. You want to give the team member time to ask questions and discuss their compensation change. Avoid trying to rush to communicate at the end of a [1:1 meeting](/handbook/leadership/1-1/).
 
* Try to clearly explain the reasoning behind the compensation change. As Compensation differentials are related to performance, the preparation done for the [Performance factor conversation](/handbook/people-group/performance-assessments-and-succession-planning/#best-practices-for-communicating-performance-factors) could also be useful in explaining the "why". It is also recommended to review the Annual Compensation review page for [all considerations that go into compensation reviews.](/handbook/total-rewards/compensation/compensation-review-cycle/) 
 
* Protect the confidentiality of other team members by avoiding saying things like “everyone else received less than you” or “you were the only team member to get a discretionary increase.”
 
* Avoid blaming others (For example: “I would have given you more but management didn’t approve.”)
 
* Avoid making future promises (For example: “In the next review, I will get you a large adjustment.”)
 
* The FY22 Total Rewards Statement includes a section on unvested equity shares. You and your direct report can use the [compensation calculator](https://comp-calculator.gitlab.net/) to review the total potential value of these shares. This is an important piece of the overall Total Rewards package at GitLab and we want to make sure team members have visibility into the potential value. [This video](https://youtu.be/ooWIfdsK5ho) is great to share with your team members when discussing the potential value of equity; it is also embedded into the compensation calculator linked above. It is important to note that all team members may perceive equity value differently based on their local tax implications, country of residence, or other factors.
 
* The FY22 Total Rewards Statement also includes a section on Benefits where team members can use the Compensation Calculator to review the value of Remote Benefits, General Benefits, and Entity-Specific benefits.
 
## Compensation Conversations Scenarios

This section provides and overview of potential compensation conversation scenarios with recommendations on how to approach and navigate these conversations. Scenarios are broken out by compensation topic: cash compensation, and equity refresh program. 
 
### Cash Compensation 

Cash compensation is discussed during our [Annual Compensation Review](/handbook/total-rewards/compensation/compensation-review-cycle/#annual-compensation-review) cycle. 

#### Scenarios
 
1. **Receiving an increase:**  A team member is receiving an adjustment based on Performance Factor AND/OR location factor/Geo Region AND/OR benchmark changes AND/OR Discretionary:
   * Sample Script:
   * "I am so pleased to inform you that you will be getting an adjustment in this year’s comp review. Your increase is based on one or more the following factors (Performance, Location Factor/Geo Region, Benchmark change and/or Discretionary). 
   * Your salary will be increased to (insert $ amount in local currency) which is a (insert %) adjustment effective, February 1st.
   * Thank you for your hard work and I look forward to continuing to work with you! Do you have any questions?"
 
2. **Not receiving an increase:**   A team member is not receiving an adjustment:
   * Sample Script:
   * "As you know, GitLab just went through our Annual Compensation Review. I wanted to inform you that you are not getting a compensation adjustment at this time. This is due to (examples below)...
       * A team member is new to the team and at the market range for your aligned role.  "Your performance is good (if applicable), however, we hire new team members at market rate for compensation so we feel that you are compensated accurately for your role at this time. You may be eligible to participate in the the [Targeted Mid-year Increase Review process](/handbook/total-rewards/compensation/compensation-review-cycle/#targeted-mid-year-increase-review)."
       * A team member is not receiving because they are above range for their role.   "Your compensation is above the market pay range for your role and therefore you are not receiving an additional increase as a part of the Annual Compensation Review. Your performance is good (if applicable), so let's discuss what you want to work on in the future and create a [development plan](/handbook/people-group/learning-and-development/career-development/) together."
       * A team member needs to improve performance. For questions on specific situations, please work with your People Business Partner.
 
   * Although informing a team member that they are not getting a compensation adjustment is a tough message to deliver, managers should have this direct conversation. This is directly aligned with our transparency value. We want everyone to know why they may not have received an adjustment and give them the space to ask questions.
 
3. **Other scenarios:** If you have a scenario different from the above, and/or you need help with messaging, please work with your manager or [People Business Partner](/handbook/people-group/#how-to-reach-the-right-member-of-the-people-group). 

### Equity Refresh Program

Eligible GitLab team members will be reviewed for a refresh equity grant once per year at the same time of the Annual Compensation Review, aligned with the process on [this page](/handbook/stock-options/#fy22-stock-option-grant-plan-design). Refresh grants use a formula to determine the range of stock options a team member may be eligible for in that cycle based on their current role/grade. The equity ranges per role can be viewed in [the compensation calculator](https://comp-calculator.gitlab.net/). Below we have outlined guidance for managers on having the conversations regarding the equity review as part of the Annual Compensation Review Cycle. 
 
*This program is subject to change over time and updates to any program elements will be reflected in the handbook.* 
 
Managers should leverage the Total Rewards Statement for communication which outlines the number of stock options that were approved by the board for the FY22 Equity Refresh. 
Definitions on the Total Rewards Statement explained: 
   * **Previous Total Unvested Stock Options:** Number of shares the team member has left to vest on their current holdings
   * **FY22 Proposed Grant:** Proposed Grant based on their equity range 
   * **One Time FY22 Transition Multiplier:** Between 1-2x depending on time since last new hire or refresh grant 
   * **FY22 Refresh Grant (Effective 2021-03-18):** Total Grant proposed by the board for FY22 taking into consideration the one time FY22 transition multiplier. 
   * **New Total Unvested Stock Options:** Number of Unvested Stock Options + FY22 Refresh Grant
 
#### Scenarios
 
1. **Receiving a refresh grant:**  A team member is receiving a refresh grant:
   * Sample Script:
   * "I am so pleased to inform you that you will be getting a refresh grant in this year’s equity refresh program. This decision has been made based on your performance and role within GitLab. 
    * **Recommended: If identified as [Key Talent](/handbook/people-group/talent-assessment/#key-talent)**: "Based on the impact you have on the business/ARR/Product/essential processes and your performance within that responsibility, I (or Senior Leadership) have identified you as Key Talent. This means we feel you fulfill a key role within GitLab and we value your skills and what you deliver to the company." 
    * "I would love for us to continue conversations to discuss what motivates you and what your career aspirations are." 
    * "Your equity refresh grant is [NUMBER]. (If applicable) This included X Multiplier factor to help transition from a three and a half year refresh to an annual refresh cycle." 
    * "Thank you for your hard work and I look forward to continuing to work with you! Do you have any questions?"

1. **Is not receiving a refresh grant, but was eligible**  A team member is not receiving any additional stock options in this equity refresh review.
    * Sample Script:
    * "As you know, GitLab just went through the Equity Refresh review cycle. I wanted to inform you that you are not getting a stock option grant at this time. This is due to (pick one of the examples below)...
    * **_A team member is currently not performing up to expectation:_** "We review equity refresh grants on a yearly basis and I want to work with you on points for improvement in order for you to meet the expectations of the role."
    * **_A team member has received an above range amount of equity at hire/as part of a former process and/or their equity levels are aligned with market:_** "With this equity review process we heavily rely on benchmarking data and our equity ranges to ensure our compensation (cash + equity + benefits) is aligned with market. At this time due to [EXAMPLE] you will not receive any additional equity. We review our equity as part of our Annual Compensation Review process and your compensation will be reviewed again at the next review cycle."

    * **A team member was eligible, _and was performing at expectations_, but is not receiving refresh grant**
 Not all team members were able to receive refresh grants, despite eligibility, as it is in the program setup/market alignment that we allocate only to a certain portion of our population. Aligned with our [expected distribution](/handbook/people-group/talent-assessment/#expected-distribution-company-wide/), typically organizations have ~25% of team members “exceeding”, 60-65% “performing”, and 10-15% “developing”. Core performers make up the most significant population in organizations, meaning that difficult decisions needed to be made in this group in terms of who was granted a refresh and who was not. 
 These were the factors considered that contributed to the decision to not allocate an additional refresh for you this year:
        * **_Recent promotion:_** "With your promotion on [DATE] you have been granted additional stock options. This time therefore you will not receive an additional option grant. However this is an annual process and we will review on our next cycle. In the meantime I would love for us to continue the conversation on how you can further progress in your new role."
        * **_Not enough improvement on identified performance/growth areas:_** "Aligned with our performance discussion in FY21 we have identified improvement areas, such as [EXAMPLES]. Between that review and this equity review, there was not sufficient improvement in the areas identified. Therefore the decision has been made to not grant you additional equity at this time. I would love for us to further discuss those improvement areas and see how I can support you in meeting all expectations going forward. We review equity grants on an annual basis, and I hope we can work together to reassess your eligibility for next cycle."
        * **_Role or skill based decision:_** "Participation in this program was based on performance and criticality of role. Therefore, not all team members will receive a refresh grant in the annual review cycle. However this is an annual process. You will be eligible to participate in next year’s program."
        * **_Not in good standing with the company:_** There could be factors that have contributed to the fact that currently the team member is not in good standing with the company. Examples here could be: Code of conduct violations or significant behavioral issues. As a communicating manager please discuss these with your People Business Partner. 
        * **_Has been with the company for a shorter period of time:_** Though only team members with less than 6 months of tenure were truly ineligible, when determining who will receive a refresh amongst a group of team members with similar performance, potential, etc., tenure may have been considered. Aligned with the equity refresh program setup and market alignment, we are only able to allocate to a certain portion of our population. Due to allocation restrictions, other factors (such as tenure) might be considered as well. 

1. **Was not eligible to be reviewed (less than 6 months of tenure)** 
	* **_Team member was hired after the cut-off date:_** "At GitLab we review compensation (cash + equity + benefits) on a yearly basis. Due to your hire date you were not eligible to receive additional compensation at this time. You will be eligible for the next Annual Compensation Review where we will review both your cash and equity compensation."
 
Although informing a team member that they are not receiving additional equity is a tough message to deliver, managers should have this direct conversation. This is directly aligned with our transparency value. We want everyone to know why they may not have received an adjustment and give them the space to ask questions.

#### Possible Reactions

1. A team member is not happy with their compensation adjustment (cash and/or equity). 
    * Listen to your team member's feedback and allow them to express their concerns.
    * Ask open ended questions to allow for the team member to share more (like what were your expectations for an adjustment this year? Why were those your expectations?)
    * Reiterate GitLab’s compensation philosophy. We pay to market based on the compensation calculator and increase compensation annually based on performance.
    * Point your team member to the Compensation Calculator where they can see the range for their role.
    * If your team member is in good standing from a performance perspective, work with them to put together a [development plan](/handbook/people-group/learning-and-development/career-development/) to help them achieve their goals (e.g. skill development or a promotion).

2. A team member is not happy with their performance factor.
    * Listen to your team member’s feedback and allow them to express their concerns.
    * Refer back to the conversation you had with your team member at the time performance factors were determined and reiterate the justification and feedback that you provided at that time.
    * If your team member is in good standing from a performance perspective, work with them to put together a [development plan](/handbook/people-group/learning-and-development/career-development/) to help them continue to grow. 

**If you need assistance after reviewing the [handbook](/handbook/total-rewards/compensation/), please work directly with your manager or your [People Business Partner](/handbook/people-group/#how-to-reach-the-right-member-of-the-people-group/)**

