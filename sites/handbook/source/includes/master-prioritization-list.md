| Priority | Description | Issue label(s) |
| ------ | ------ | ------ |
| 1* | <a href="/handbook/engineering/security/#severity-and-priority-labels-on-security-issues">Security fixes</a> | `security` |
| 2 | Data-loss prevention | `data loss` |
| 3* | <a href="/handbook/engineering/performance/index.html#availability">Availability</a>, <a href="/handbook/engineering/workflow/#infradev">Infradev</a>, Incident Corrective Actions, [Sharding Blockers](https://about.gitlab.com/company/team/structure/working-groups/database-scalability/) | `availability`, `infradev`, `Corrective Action`, `sharding-blocker`  |
| 4 | Fixing regressions (things that worked before) | `regression` |
| 5 | Performance Refinement | `performance-refinement` |
| 6 | [Issues Prioritized for Specific Customers](/handbook/product/product-processes/#issues-important-to-customer) | `planning priority` |
| 7 | Instrumentation improvements, particularly for xMAU | `instrumentation` |
| 8 | Usability Improvements and User Experience to drive xMAU |`feature::enhancement`, `UX debt`|
| 9 | IACV Drivers | |
| 10 | Identified for Dogfooding | `Dogfooding::Build in GitLab`, `Dogfooding::Rebuild in GitLab` |
| 11 | Velocity of new features, technical debt, community contributions, and all other improvements | `direction`, `feature`, `technical debt` |
| 12 | Behaviors that yield higher predictability (because this inevitably slows us down) | `predictability` |

*indicates forced prioritization items with SLAs/SLOs

### Engineering Allocation

Engineering is the DRI for mid/long term team efficiency, performance, security (incident response and anti-abuse capabilities), availability, and scalability. The expertise to proactively identify and iterate on these is squarely in the Engineering team. Whereas Product can support in performance issues as identified from customers. In some ways these efforts can be viewed as risk-mitigation or revenue protection. They also have the characteristic of being larger than one group at the stage level. Development would like to conduct an experiment to focus on initiatives that should help the organization scale appropriately in the long term.  We are treating these as a percent investment of time associated with a stage or category. The percent of investment time can be viewed as a prioritization budget outside normal Product/Development assignments.

Engineering Allocation is also used in short-term situations in conjunction and in support of maintaining acceptable Error Budgets for GitLab.com and our [GitLab-hosted first](/direction/#gitlab-hosted-first) theme.

Unless it is listed in this table, Engineering Allocation for a stage/group is 0% and we are following normal [prioritization](/handbook/product/product-processes/#prioritization). Refer to this [page](https://about.gitlab.com/handbook/engineering/engineering-allocation/) for Engineering Allocation charting efforts. Some stage/groups may be allocated at a high percentage or 100%, typically indicating a situation where all available effort is to be focused on Reliability related (top 5 priorities from [prioritization table](/handbook/product/product-processes/#prioritization)) work.

Mid/long term initiatives are engineering-led. The EM is responsible for recognizing the problem, creating a satisfactory goal with clear success criteria, developing a plan, executing on a plan and reporting status.  It is recommended that the EM collaborate with PMs in all phases of this effort as we want PMs to feel ownership for these challenges.  This could include considering adding more/less allocation, setting the goals to be more aspirational, reviewing metrics/results, etc.   We welcome strong partnerships in this area because we are one team even when allocations are needed for long-range activities.

During periods of Engineering Allocation, the PM remains the interface between the group and the fields teams & customers. This is important because:
- It allows Engineering to remain focused on the work at hand
- It maintains continuity for the field teams - they should not have to figure out different patterns of communication for the customer
- It keeps PMs fully informed about the product's readiness

| Group/Stage | Description of Goal | Justification | Maximum % of headcount budget | People | Supporting information | EMs / DRI | PMs |
| ------ | ------ | ------- | ------ | ------ | ------- |  ------ | ------ |
| Manage:Authentication and Authorization (BE)  | Dev Security burn-down | More security issues exist in the backlog and are incoming than the team is able to prioritize |  50%  | 2 | [Overview](https://gitlab.com/gitlab-org/gitlab/-/issues/341883) | @dennis | @ogolowinski  |
| Manage:Authentication and Authorization (BE)  | 3 month headcount reset to help Manage:Workspace | 3 month headcount reset to help Manage:Workspace |  25% | 1 | 3 month headcount reset to help Manage:Workspace | @lmcandrew  | @hsutor   |
| Manage:Workspace (BE)  | Scalability of GitLab hierarchy functionality (Workspace) | Reduce duplication of code and increase performance for Groups/Projects |  0% | 0 | [Consolidate Groups and Projects](https://gitlab.com/groups/gitlab-org/-/epics/6473) | @mksionek  | @mushakov |
| Manage:Workspace (BE)  | Linear Namespace Queries | Replace recursive CTE queries, which are complex and unpredictable |  50% | 1 | [Linear Namespace Queries](https://gitlab.com/groups/gitlab-org/-/epics/5296) | @mksionek | @ogolowinski  |
| Manage:Workspace (BE) | Dev Security burn-down | Burn down Dev security backlogs down | 50% | 1 | [Overview](https://gitlab.com/gitlab-org/gitlab/-/issues/341883) | @mksionek | @ogolowinski |
| Manage:Compliance (BE) | floor % | empower every SWEs from raising reliability and security issues | 10% | 1 | N/A | @djensen | @stkerr |
| Manage:Import (BE)  | Improving Error Budget, Infra-Dev Issues, and Security |  Backlog of security issues and production incidents tied to infradev | 100% | 2 | [Infradev & Security Issues](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&state=opened&label_name[]=group%3A%3Aimport&label_name[]=Engineering%20Allocation) | @lmcandrew |  @hdelalic  |
| Manage:Optimize (BE) | Dev Security burn-down | Burn down Dev security backlogs down |  67% | 3 | [Overview](https://gitlab.com/gitlab-org/gitlab/-/issues/341883) | @djensen | @hdelalic |
| Plan:Project management | 3 month headcount reset to help Manage:Workspace | 3 month headcount reset to help Manage:Workspace | 50% | 4 | 3 month headcount reset to help Manage:Workspace | @jlear | @gweaver |
| Plan:Product Planning | 3 month headcount reset to help Manage:Workspace | 3 month headcount reset to help Manage:Workspace | 40% | 5 | 3 month headcount reset to Manage:Workspace | @johnhope | @cdybenko |
| Create:Source Code (BE) | Dev Security burn-down | Burning down dev security issues| 100% | 3 | [Overview](https://gitlab.com/gitlab-org/gitlab/-/issues/341883) | @sean_carroll | @sarahwaldner    |
| Create:Code Review (BE) | Dev Security burn-down | Burn down Dev security backlogs down | 100% | 5 | [Overview](https://gitlab.com/gitlab-org/gitlab/-/issues/341883) | @mnohr | @phikai |
| Create:Editor (BE) | Infradev, Linear Queries, Dev Security burn-down | Close all infradev issues, work through linear queries epic, Burn down Dev security backlogs down | 100% | 2 | [Overview](https://gitlab.com/gitlab-org/gitlab/-/issues/341883) | @oregand | @ericschurter |
| Create:Gitaly | Infra-Dev Issues, P1/S1 issues, security issues and Customer Escalations (engineering approved) | Improve reliability of Gitaly |  20%  | 6 |  [Infradev Issues](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&state=opened&label_name[]=infradev&label_name[]=group%3A%3Agitaly) | @timzallmann | @mjwood |
| Ecosystems:Integrations | Dev Security burn-down | Burn down Dev security backlogs down | 100% | 4 | [Overview](https://gitlab.com/gitlab-org/gitlab/-/issues/341883) | @arturoherrero | @mushakov |
| Verify:Pipeline Execution (BE) | Burndown of sharding-blockers, infradev and Security issues | Support Data Decomposition efforts, continue supporting Reliability in the Veriy stage | 100% | 3 | [Verify priorities proposal for FY22-Q4](https://gitlab.com/gitlab-org/verify-stage/-/issues/145) | @cheryl.li |  @jheimbuck_gl   |
| Verify:Pipeline Authoring (BE) | Burndown of sharding-blockers, infradev and Security issues | Support Data Decomposition efforts, continue supporting Reliability in the Veriy stage | 66% | 2 | [Verify priorities proposal for FY22-Q4](https://gitlab.com/gitlab-org/verify-stage/-/issues/145) | @cheryl.li |  @dhershkovitch  |
| Verify:Pipeline Authoring (BE) | floor % | empower every SWEs from raising reliability and security issues | 10% | 3 | N/A | @marknuzzo | @dhershkovitch |
| Verify:Runner | floor % | empower every SWEs from raising reliability and security issues |  10% | 6 | N/A | @erushton | @DarrenEastman |
| Verify:Testing (BE) | Burndown of sharding-blockers, infradev and Security issues | Support Data Decomposition efforts, continue supporting Reliability in the Veriy stage | 100% | 2 | [Verify priorities proposal for FY22-Q4](https://gitlab.com/gitlab-org/verify-stage/-/issues/145) | @cheryl.li |  @jreporter   |
| Package:Package | floor % | empower every SWEs from raising reliability and security issues | 10% | 5 | N/A | @dcroft | @trizzi |
| Release:Release | 3 milestones manage:Import Headcount Reset | Unlocks a new CEO initiative | 17% | 1 | https://gitlab.com/gitlab-com/Product/-/issues/3062 | @nicolewilliams | @cbalane |
| Configure:Configure | 3 milestones manage:Import Headcount Reset | Unlocks a new CEO initiative  | 20% | 1 | https://gitlab.com/gitlab-com/Product/-/issues/3062 | @nicholasklick | @nagyv-gitlab |
| Verify | Improve GitLab.com CI capacity visibility and Shared Runners ability to handle increased load | Give us 4-5 years of runway | 20% | 3 | [CI Scaling Target](https://gitlab.com/groups/gitlab-org/-/epics/5745), [CI/CD Capacity Planning](https://gitlab.com/groups/gitlab-org/-/epics/6927) | @grzesiek  | @jreporter |
| Secure:Static Analysis | floor % | empower every SWEs from raising reliability and security issues | 10% | 5 | N/A | @twoodham | @connorgilbert |
| Secure:Dynamic Analysis | floor % | empower every SWEs from raising reliability and security issues | 10% | 5 | N/A | @sethgitlab | @derekferguson |
| Secure:Composition Analysis | Proposed 3 month headcount reset to help manage:Import | Proposed 3 month headcount reset to help manage:Import | 25% | 4 | Proposed 3 month headcount reset to help manage:Import | @gonzoyumo  | @NicoleSchwartz |
| Secure:Threat Insights | Bring error budget back to green | Work on backlog of reliability and security issues | 25% | 4 | [List of issues](https://gitlab.com/gitlab-org/gitlab/-/issues/299275#saas-reliability-focus-for-q3fy22) | @thiagocsf | @matt_wilson |
| Protect:Container Security | floor % | empower every SWEs from raising reliability and security issues | 10% | 5 | N/A | @thiagocsf | @sam.white |
| Growth:Activation | floor % | empower every SWEs from raising reliability and security issues | 10% | 1 | N/A | @pcalder | @jstava |
| Growth:Conversion | floor % | empower every SWEs from raising reliability and security issues | 10% | 2 | N/A | @pcalder | @s_awezec |
| Growth:Expansion | 3 month headcount reset to help manage: Import | 3 month headcount reset to help manage: Import | 50% | 2 | 3 month headcount reset to help manage: Import | @pcalder | @gdoud |
| Growth:Adoption | floor % | empower every SWEs from raising reliability and security issues | 10% | 2 | N/A | @pcalder | @mkarampalas |
| Growth:Product Intelligence | floor % | empower every SWEs from raising reliability and security issues | 10% | 6 | N/A | @nicolasdular | @amandarueda |
| Fulfillment | Improve availability of CustomersDot by migrating from Azure to GCP | Improve availability of CustomersDot due to [several recent outages](https://gitlab.com/gitlab-com/gl-infra/production/-/issues?scope=all&utf8=%E2%9C%93&state=all&label_name[]=Service%3A%3ACustomers) | 20% (4 Fulfillment Engineers + 0.67 Infrastructure Engineers) | 20 | [Epic](https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/390) | @jeromezng | @justinfarris |
| Enablement:Distribution | floor % | empower every SWEs from raising reliability and security issues | 10% | 9 | N/A | @mendeni | @dorrino |
| Enablement: Geo | 3 months headcount reset to new staging environment | 3 months headcount reset to new staging environment | 13% | 7 | 3 months headcount reset to new staging environment | @nhxnguyen | @nhxnguyen |
| Enablement:Database | Primary Key overflow, Retention Strategy, Schema Validation, migration improvements, testing | Database has been under heavy operational load and needs improvement | 100% | 5 | [Automated Migration testing](https://gitlab.com/groups/gitlab-org/database-team/-/epics/6), [Automated migrations for primary key conversions](https://gitlab.com/groups/gitlab-org/-/epics/5654), [Remove PK overflow](https://gitlab.com/groups/gitlab-org/-/epics/4785), [Schema Validation](https://gitlab.com/groups/gitlab-org/-/epics/3928), [Reduce Total size of DB](https://gitlab.com/groups/gitlab-org/-/epics/4181) | @craig-gomes | TBD |
| Enablement:Sharding | floor % | empower every SWEs from raising reliability and security issues | 10% | 4 | N/A | @craig-gomes | @fzimmer |
| Enablement:Memory | Improve Redis Scalability | Redis is one of our top scaling bottlenecks | 50% | 2 | [Create dedicated Redis instance for session keys](https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/579) | @changzhengliu | @iroussos |
| Enablement:Global Search | Enhance security in GitLab application | Security is the top priority in [Prioritizing technical decisions](https://about.gitlab.com/handbook/engineering/#prioritizing-technical-decisions) | 10% | 1 | [Security related issue](https://gitlab.com/gitlab-org/gitlab/-/issues/339169) | @changzhengliu | @JohnMcGuire |

#### Broadcasting and communication of Engineering Allocation direction

Each allocation has a [direction page](/handbook/product/product-processes/#managing-your-product-direction) maintained by the Engineering Manager. The Engineering Manager will provide regular updates to the direction page. Steps to add a direction page are:

1. Open an MR to the [direction content](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/source/direction/)
1. Add a directory under the correct stage named for the title Engineering Allocation
1. Add a file for the page named `index.html.md` in the newly created directory

To see an example for an Engineering Allocation Direction page, see [Continuous Integration Scaling](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/source/direction/verify/continuous_integration_scaling/index.html.md). Once the Engineering Allocation is complete, delete the direction page.

#### Communicating Engineering Allocation Progress

Groups allocating effort to an engineering allocation should update progress synchronously or asynchronously in the weekly, cross-functional infradev and engineering allocation meeting [[agenda](https://docs.google.com/document/d/1j_9P8QlvaFO-XFoZTKZQsLUpm1wA2Vyf_Y83-9lX9tg/edit#bookmark=id.tr8ld1ht454z) (internal)]. The intention of this meeting is to communicate progress on engineering allocations and to evaluate and prioritise escalations from infrastructure.

Engineering Allocation progress reports should appear in the following format:

1. DRI person: Short engineering allocation description (including link if possible) **Not Verbalized**
   1. Target date:
   1. Percent done:
   1. Context:
      - incident link
      - RCA link
      - Epic/meta-issue link
      - Other context
   1. Status: **Verbalized**
      - On pace (confidence?)
      - next steps / blockers / significant changes and/or wins

#### How to get a effort added to Engineering Allocation

One of the most frequent questions we get as part of this experiment is "How does a problem get put on the Engineering Allocation list?".  The short answer is someone makes a suggestion and we add it.  Much like everyone can contribute, we would like the feedback loop for improvement and long terms goals to be robust.  So everyone should feel the empowerment to suggest an item at any time.

To help with getting items that on the list for consideration, we will be performing a survey periodically.  The survey will consist of the following questions:

1. If you were given a % of engineering development per release to work on something, what would it be?
1. How would you justify it?

We will keep the list of questions short to solicit the most input.  The survey will go out to members of the Development, Quality, Security.  After we get the results, we will consider items for potential adding as an Engineering Allocation.


#### Closing out Engineering Allocation items

Once the item's success criteria are achieved, the Engineering Manager should consult with counterparts to review whether the improvements are sustainable. Where appropriate, we should consider adding monitoring and alerting to any areas of concern that will allow us to make proactive prioritizations in future should the need arise. The Engineering Manager should close all related epics/issues, reset the allocation in the above table to the floor level, and inform the Product Manager when the allocated capacity will be available to return their focus to product prioritizations.

When reseting a groups Engineering Allocation in the table above, the goal should be set as `floor %`, the goal should be `empower every SWEs from raising reliability and security issues`, percentange of headcount allocated should be `10%`, and `N/A` in place of a link to the Epic.

All engineering allocation closures should be reviewed and approved by the [VP of Development](https://about.gitlab.com/handbook/engineering/development/#team-members).

### Feature Change Locks

We will enact a localized feature change lock (FCL) anytime there is an S1 or public-facing (tweeted) S2 incident on GitLab.com (including the License App, CustomersDot, and Versions) determined to be caused by a change from the development department. The [team](https://about.gitlab.com/company/team/structure/#organizational-structure) involved should be determined by the author, their line manager, and that manager's other direct reports. An FCL assignment and creation must be approved by either the VP of Infrastructure or VP of Development. The intent is to create a sense of ownership and accountability amongst our teams, but this should not challenge our no-blame culture.

The FCL will last 5 business days. During this time, the team(s) exclusive focus is around reliability work, and any other type of work in-flight has to be paused.  The team(s) must:

* Create a public slack channel called `#fcl-incident-[number]`, with members
    * The Team's Manager
    * The Author and their teammates
    * All reviewer(s)
    * All maintainers(s)
    * Infrastructure Stable counterpart
    * The chain-of-command from the manager to the CTO (Sr Manager, Sr/Director, VP of Development, CTO, etc)
* Create an FCL epic or issue with the information below in the description
    * Name the epic or issue: [Group Name] FCL for Incident ####
    * FCL time period (start date - end date)
    * Link to the production incident issue
* Complete the written Incident Review documentation within the Incident Issue as the first priority after the incident is resolved.  The Incident Review must include completing all fields in the Incident Review section of the incident issue (see [incident issue template](https://gitlab.com/gitlab-com/gl-infra/production/-/blob/master/.gitlab/issue_templates/incident.md)).  The incident issue should serve as the single source of truth for this information, unless a linked confidential issue is required. Completing it should create a common understanding of the problem space and set a shared direction for the work that needs to be completed.
* See that not only all procedures were followed but also how improvements to procedures could have prevented it
* A work plan referencing all the Issues, Epics, and/or involved MRs must be created and used to identify the scope of work for the FCL. The work plan itself should be an Issue or Epic.
* Report daily to the Reliability & Security Stand-up using the template:
    * Exec-level summary
        * Date Started
        * Target End Date
        * Link to FCL Issue/Epic
        * Link to Incident Issue
        * Link to RCA Artifact
        * Link to Causal MR/Issue
        * [YYYY-MM-DD] update:
            * Highlights/lowlights
* Hold a synchronous `closing ceremony` upon completing the FCL to review the retrospectives and celebrate the learnings.
    * All FCL stakeholders and participants shall attend or participate async.  Managers of the groups participating in the FCL, including Sr. EMs and Directors should be invited.
    * Agenda includes reviewing FCL retrospective notes and sharing learnings about improving code change quality and reducing risk of availability.
    * Outcome includes [handbook](https://about.gitlab.com/handbook/) and [GitLab Docs](https://docs.gitlab.com/ee/) updates where applicable.

After the Incident Review is completed, the team(s) focus is on:

* Address immediate corrective actions to prevent incident reoccurrence in the short term
* Introduce changes to reduce incident detection time (Improve collected metrics, and the service level monitoring)
* Introduce changes to reduce mitigation time (Improve rollout process through feature flags, and clean rollbacks)
* Ensure that the incident is reproducible in environments outside of production (Detect issues in staging, increase end-to-end integration test coverage)
* Improve development test coverage (Harden unit testing, make it simpler to detect problems during reviews)
* Create issues with general process improvements or asks for other teams

Examples of this work include, but are not limited to:

* Fixing items from the Incident Review which are identified as causal or contributing to the incident.
* Improving observability
* Improving unit test coverage
* Adding integration tests
* Improving service level monitoring
* Improving symmetry of pre-production environments
* Improving the [GitLab Performance Tool](https://gitlab.com/gitlab-org/quality/performance)
* Adding mock data to tests or environments
* Making process improvements
* Populating their backlog with further reliability work
* Security work
* Improve communication and workflows with other teams or counterparts

Any work for the specific team kicked off during this period must be completed, even if it takes longer than the duration of the FCL. Any work directly related to the incident should be kicked off and completed even if the FCL is over. Work paused due to the FCL should be the priority to resume after the FCL is over. Items created for other teams or on a global level don't affect the end of the FCL.

The stable counterpart from Infrastructure will be available to review and consult on the work plan.

This is in effect as of September 2, 2021 (retroactive) for six months. R&D leadership will evaluate Mar 1, 2022 and decide whether to continue, modify, or cancel the process (already scheduled).
